import { Icon } from 'local/react-ui/icons'
import { Tooltip } from 'local/react-ui/tooltip'
import { component, React, Style } from 'local/react/component'

export const defaultSocialLinkOrder = [
  'linkedIn',
  'instagram',
  'facebook',
  'twitter',
  'email'
]

export const defaultSocialStyle: Style = {
  display: 'inline-block',
  verticalAlign: 'middle'
}

export const defaultFinalSocialLinkStyle: Style = {
  display: 'inline-block',
  verticalAlign: 'middle',
  textDecoration: 'none',
  color: 'inherit',
  textAlign: 'center',
  position: 'relative',
  height: '19px'
}

export const defaultSocialLinkStyle: Style = {
  ...defaultFinalSocialLinkStyle,
  marginRight: '1rem'
}

export const defaultSocialLinkTextStyle: Style = {
  display: 'none'
}

export const defaultSocialIconStyle: Style = {
  color: 'inherit',
  fontSize: '130%',
  verticalAlign: 'middle',
  height: '2.5rem',
  overflow: 'hidden'
}

export const Social = component
  .props<{
    style?: Style
    linkStyle?: Style
    linkTextStyle?: Style
    finalLinkStyle?: Style
    iconStyle?: Style
    facebook?: string
    twitter?: string
    instagram?: string
    linkedIn?: string
    email?: string
    linkOrder?: string[]
  }>({
    finalLinkStyle: defaultFinalSocialLinkStyle,
    iconStyle: defaultSocialIconStyle,
    linkOrder: defaultSocialLinkOrder,
    linkStyle: defaultSocialLinkStyle,
    linkTextStyle: defaultSocialLinkTextStyle,
    style: defaultSocialStyle
  })
  .render(({ style, ...props }) => (
    <div style={style}>{renderLinks(props)}</div>
  ))

const renderLinks = ({
  linkStyle,
  linkTextStyle,
  finalLinkStyle,
  iconStyle,
  facebook,
  twitter,
  instagram,
  linkedIn,
  email,
  linkOrder
}: {
  linkStyle?: Style
  linkTextStyle?: Style
  finalLinkStyle?: Style
  iconStyle?: Style
  facebook?: string
  twitter?: string
  instagram?: string
  linkedIn?: string
  email?: string
  linkOrder?: string[]
}) => {
  const values = {
    facebook,
    twitter,
    instagram,
    linkedIn,
    email
  }

  const order = linkOrder.filter(name => typeof values[name] === 'string')

  return order.map((name, index) =>
    renderLink[name]({
      linkStyle: index === order.length - 1 ? finalLinkStyle : linkStyle,
      value: values[name],
      iconStyle,
      linkTextStyle
    })
  )
}

interface IRenderLinkProps {
  iconStyle: Style
  linkStyle: Style
  linkTextStyle: Style
  value: string
}

const renderLink = {
  linkedIn: ({
    linkStyle,
    value,
    iconStyle,
    linkTextStyle
  }: IRenderLinkProps) => (
    <a
      key="linkedIn"
      target="_blank"
      style={linkStyle}
      href={`https://www.linkedin.com/in/${value}/`}
    >
      <Icon style={iconStyle} brand={true} name="linkedin" />
      <span style={linkTextStyle}>LinkedIn</span>
      <Tooltip text={`Visit ${value} on LinkedIn`} />
    </a>
  ),
  instagram: ({
    linkStyle,
    value,
    iconStyle,
    linkTextStyle
  }: IRenderLinkProps) => (
    <a
      key="instagram"
      target="_blank"
      style={linkStyle}
      href={`https://www.instagram.com/${value}/`}
    >
      <Icon style={iconStyle} brand={true} name="instagram" />
      <span style={linkTextStyle}>Instagram</span>
      <Tooltip text={`Visit ${value} on Instagram`} />
    </a>
  ),
  facebook: ({
    linkStyle,
    value,
    iconStyle,
    linkTextStyle
  }: IRenderLinkProps) => (
    <a
      key="facebook"
      target="_blank"
      style={linkStyle}
      href={`https://www.facebook.com/${value}`}
    >
      <Icon style={iconStyle} brand={true} name="facebook" />
      <span style={linkTextStyle}>Facebook</span>
      <Tooltip text={`Visit ${value} on Facebook`} />
    </a>
  ),
  twitter: ({
    linkStyle,
    value,
    iconStyle,
    linkTextStyle
  }: IRenderLinkProps) => (
    <a
      key="twitter"
      target="_blank"
      style={linkStyle}
      href={`https://twitter.com/${value}`}
    >
      <Icon style={iconStyle} brand={true} name="twitter" />
      <span style={linkTextStyle}>Twitter</span>
      <Tooltip text={`Visit @${value} on Twitter`} />
    </a>
  ),
  email: ({ linkStyle, value, iconStyle, linkTextStyle }: IRenderLinkProps) => (
    <a key="email" style={linkStyle} href={`mailto:${value}`}>
      <Icon style={iconStyle} name="envelope-square" />
      <span style={linkTextStyle}>Email</span>
      <Tooltip text={`Email ${value}`} />
    </a>
  )
}
